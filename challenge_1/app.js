const app = Vue.createApp({
    data () {
        return {
            name: "Krystian",
            age: 25,
            sampleImg: "https://2.img-dpreview.com/files/p/E~C1000x0S4000x4000T1200x1200~articles/3925134721/0266554465.jpeg",
        };
    },
    methods: {
        drawNumber() {
            return Math.random();
        }
    }
});

app.mount('#assignment');
